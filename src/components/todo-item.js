import React, { Component } from "react";
import PropTypes from "prop-types";

import { TodoItemElm } from "./styled";
import TodoItemActions from "./todo-item-actions";
import { TODO_STATUS } from "../domains/todo";

class TodoItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      actionsBarIsDisplaying: false
    };
  }

  render() {
    const { id, content, status } = this.props;
    let todoItemStatusClass = 
            status === TODO_STATUS.COMPLETED ? "completed" : "";

    return (
      <TodoItemElm
        className={todoItemStatusClass}
        data-id={id}

        onContextMenu={e => {
          e.preventDefault();
          e.stopPropagation();
          this.setState({
            actionsBarIsDisplaying: true
          });
          console.log(this.state);
        }}

        onMouseLeave={e => {
          setTimeout(() => {
            this.setState({
              actionsBarIsDisplaying: false
            });
          }, 200);
        }}

        onClick={e => {
          e.preventDefault();
          if (this.state.actionsBarIsDisplaying) {
            this.setState({
              actionsBarIsDisplaying: false
            });
          }
        }}
      >
        {content}

        {this.state.actionsBarIsDisplaying 
          ? (<TodoItemActions {...this.props} hasCompleted={status} />) 
          : ("")}
          
      </TodoItemElm>
    );
  }
}

TodoItem.propTypes = {
  id: PropTypes.number.isRequired,
  content: PropTypes.string,
  status: PropTypes.symbol
};

export default TodoItem;
