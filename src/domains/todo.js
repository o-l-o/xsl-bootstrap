export class Todo {
  constructor(id = new Date().getMilliseconds(), content = "", status = false) {
    this.id = id;
    this.content = content;
    this.status = status;
  }
}

export const TODO_STATUS = {
  COMPLETED: Symbol("todo item has been completed"),
  UNDO: Symbol("todo is undo")
};
