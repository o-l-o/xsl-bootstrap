import { TODO_STATUS } from "./todo";

export const CHANGE_TODO_ITEM_STATUS_ACTION = Symbol(
  "Action: 改变 Todo项 的状态"
);
export const REGISTER_TODO_ITEM_ACTION = Symbol("Action: 注册新的 Todo项");
export const REVORK_TODO_ITEM_ACTION = Symbol("Action: 注销一个 Todo项");
export const SET_VISIBILITY_FILTER_ACTION = Symbol("Action: 设置过滤器");

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_ACTIVE: "SHOW_ACTIVE"
};

/*
 * action 创建函数
 */

export function registerTodoItem(todo) {
  return { type: REGISTER_TODO_ITEM_ACTION, todo };
}

export function revorkTodoItem(todo) {
  return { type: REVORK_TODO_ITEM_ACTION, todo };
}

export function changeTodoItemStatus(todo) {
  return { type: CHANGE_TODO_ITEM_STATUS_ACTION, todo };
}

export function setTodoItemsVisibilityFilter(filter) {
  return { type: SET_VISIBILITY_FILTER_ACTION, filter };
}

export const filterTodoItems = (todos, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(t => t.status === TODO_STATUS.COMPLETED);
    case VisibilityFilters.SHOW_ACTIVE:
      return todos.filter(t => t.status === TODO_STATUS.UNDO);
    default:
      return todos;
  }
};

const mapStateToProps = state => {
  return {
    todos: filterTodoItems(state.todos, state.visibilityFilter)
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(setTodoItemsVisibilityFilter(ownProps.filter));
    }
  };
};
