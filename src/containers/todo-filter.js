import React, { Component } from "react";
import { connect } from "react-redux";
import {
  changeTodoItemStatus,
  revorkTodoItem,
  setTodoItemsVisibilityFilter
} from "../domains/actions";
import TodoList from "../components/todo-list";
import { TODO_STATUS } from "../domains/todo";
import TodoFilterLink from "../components/todo-filter-link";

const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case "SHOW_COMPLETED":
      return todos.filter(t => t.status === TODO_STATUS.COMPLETED);
    case "SHOW_ACTIVE":
      return todos.filter(t => t.status === TODO_STATUS.UNDO);
    case "SHOW_ALL":
    default:
      return todos;
  }
};

const mapStateToProps = state => {
  return {
    todos: getVisibleTodos(state.todos, state.visibilityFilter)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTodoItemStatusChange: todo => {
      dispatch(changeTodoItemStatus(todo));
    },
    onTodoItemBeRemoved: todo => {
      dispatch(revorkTodoItem(todo));
    },
    onTodoItemsVisibilityFilterChange: filter => {
      dispatch(setTodoItemsVisibilityFilter(filter));
    }
  };
};

class VisibleTodoList extends Component {
  render() {
    return (
      <div>
        <TodoFilterLink {...this.props} />
        <TodoList {...this.props} />
      </div>
    );
  }
}

VisibleTodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(VisibleTodoList);

export default VisibleTodoList;
